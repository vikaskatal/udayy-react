import React from 'react';
import logo from '../logo.png';

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            showAlert: false
        };
    }

    login = () => {
        const {
            username,
            password,
        } = this.state;

        if(username.length > 3 && password=='1234'){
            localStorage.setItem('user', username);
            this.props.checkUserLogin()

        }else{
            this.setState({
                showAlert: true
            })
        }
    };

    handleChange = (event) => {
        const { target: { name, value } } = event
        this.setState({ [name]: value })
    };

    render() {
        return <div className="container">

            <div className="login">
                <div>
                    <img src={logo} alt="Logo" />
                    <div className="sec-login__container">
                        <h2>Login </h2>

                        {
                            this.state.showAlert ? <div className="alert alert-danger">Invalid credentials</div> : null
                        }

                        <div className="form-group">
                            <label>User name</label>
                            <input
                                name="username"
                                className="form-control"
                                type="text"
                                value={this.state.username}
                                onChange={this.handleChange} />
                        </div>
                        <div className="form-group">
                            <label>Password</label>
                            <input
                                name="password"
                                className="form-control"
                                type="password"
                                value={this.state.password}
                                onChange={this.handleChange} />
                        </div>

                        <button className="btn btn-primary" onClick={this.login}>Submit</button>

                        <div className="note">Mock Username length should be greater than 3 and Password is '1234'</div>
                    </div>
                </div>
            </div>

        </div>
    }
}
export default Login;
