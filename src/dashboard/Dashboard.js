import React from 'react';
import logo from '../logo.png';
import Viewer from './viewer/Viewer';

class Dashboard extends React.Component {
    constructor(props) {
        super(props);
    }

    logout = () => {
        localStorage.removeItem('user');
        this.props.checkUserLogin();
    };

    render() {
        return <div>

            <header className="app-header ">
                <div className="container">
                    <div className="d-flex justify-content-between align-items-center">
                        <img className="app-header__logo" src={logo} alt="Logo" />

                        <div className="app-header__items">
                            <p className="app-header__user">{this.props.user}</p>
                            <p className="app-header__logout cursor-pointer" onClick={this.logout}> Logout</p>
                        </div>
                    </div>
                </div>
            </header>


            <div className="container-fluid p-0">
                <div className="view d-flex">
                    <Viewer />
                    <Viewer />
                </div>
            </div>

        </div>;
    }
}

export default Dashboard;
