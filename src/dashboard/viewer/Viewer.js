import React from 'react';

class Viewer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            view: false,
            url: ''
        };
    }

    urlValidation = (str) => {
        let regexp =  /^(?:(?:https?|ftp):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/;
        if (regexp.test(str)) {

            return true;
        } else {
            console.log('not valid')
            return false;
        }
    };

    handleKeyDown = (e) => {
        if (e.key === 'Enter') {
            if(this.urlValidation(e.target.value)){
                this.setState({
                    url : e.target.value,
                    view : true
                })
            }else{
                alert('Url not valid')
                this.setState({
                    url : '',
                    view : false
                })
            }
        }
    };


    render() {
        return <div className="view__item">
            <div className="view__input">
                <input className="form-control" onKeyDown={this.handleKeyDown} type="text" placeholder="Enter Url"/>
            </div>
            <div className="view__area">
                {
                    this.state.view ?
                        <div>
                            <iframe src={this.state.url}>
                                <p>Your browser does not support iframes.</p>
                            </iframe>
                        </div> :
                        <div className="view__preview d-flex align-items-center justify-content-center">
                            <p>Enter url to <span>Preview</span></p>
                        </div>
                }
            </div>
        </div>;
    }
}

export default Viewer;
