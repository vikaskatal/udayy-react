import React from 'react';
import './App.scss';
import Login from './auth/Login';
import Dashboard from './dashboard/Dashboard';


class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            user: '',
            isLogin: false
        };
    }

    componentDidMount() {
        this.checkUserExist();
    };

    checkUserExist = () => {
        let user = localStorage.getItem('user');
        if(user){
            this.setState({
                isLogin: true,
                user: user
            })
        }else{
            this.setState({
                isLogin: false
            })
        }
    };

    checkUserLogin = () => {
        this.checkUserExist();
    };

    render() {
        return <div>
            {
                this.state.isLogin ?
                    <Dashboard user={this.state.user} checkUserLogin={this.checkUserLogin} /> :
                    <Login checkUserLogin={this.checkUserLogin} />
            }
        </div>;
    }
}



export default App;

